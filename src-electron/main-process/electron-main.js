import { app, BrowserWindow } from 'electron';

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\');
}

const mainWindows = [];

function createWindow() {
  /**
   * Initial window options
   */
  for (let i = 0; i < 2; i++) {
    let mainWindow = new BrowserWindow({
      width: 500,
      height: 600,
      x: (100 + 550 * i),
      y: 100,
      useContentSize: true,
    });

    mainWindow.loadURL(process.env.APP_URL);

    mainWindow.on('closed', () => {
      mainWindow = null;
    });
    mainWindows.push(mainWindow);
  }
}

global.sharedObject = {
  innerVal: 'rapidly share data between windows',
  innerValMutator: (newVal) => {
    global.sharedObject.innerVal = newVal;
    mainWindows.forEach((win) => {
      win.send('sharedUpdate', global.sharedObject.innerVal);
    });
    console.log('updated in main');
  },
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
